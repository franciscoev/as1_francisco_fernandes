﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace CommaSprinkler
{
    class CommaSprinklerVM : INotifyPropertyChanged
    {
        private int step = 0;
        public string OriginalStr { get; set; } = "";
        public string ExpectedStr { get; set; } = null;
        public string ModifiedStr { get; set; } = "";
        public BindingList<string> Log { get; set; } = new BindingList<string>();

        private const string PATTERN_COMMA_AFTER = "\\s?\\w*\\,";
        private const string PATTERN_MODIFY_COMMA_AFTER = "(\\s|^(\\.|\\,))";
        private const string PATTERN_REPLACEMENT_COMMA_AFTER = "$1, ";

        private const string PATTERN_COMMA_BEFORE = "\\,\\s\\w*";
        private const string PATTERN_MODIFY_COMMA_BEFORE = "(\\w)(\\s)";
        private const string PATTERN_REPLACEMENT_COMMA_BEFORE = "$1, $3";
        public CommaSprinklerVM()
        {

        }
        public CommaSprinklerVM(string originalStr)
        {
            this.OriginalStr = originalStr;
        }

        public CommaSprinklerVM(string originalStr, String expectedStr)
        {
            if (originalStr != null)
                this.OriginalStr = originalStr;
            if (expectedStr != null)
                this.ExpectedStr = expectedStr;
        }

        private List<string> FindWordsWithComma(string workStr, string pattern2FindWord)
        {
            int count = 0;

            List<string> matchedList = new List<string>();
            foreach (Match mWord in Regex.Matches(workStr, pattern2FindWord))
            {
                Log.Add("[" + step + "." + count++ + "] Matched Word : '" + mWord + "', starting at : " + mWord.Index);
                matchedList.Add(Regex.Replace(mWord.ToString(), "\\W", ""));
            }

            return matchedList;
        }

        private string ModifyStr(string workStr, string pattern2Replace, string pattern2Replacement)
        {
            int count = 0;

            MatchCollection mc = Regex.Matches(workStr, pattern2Replace);
            foreach (Match m in mc)
            {
                // Just to format for better presentation on the Log File
                Log.Add("[Find2Modify " + step + "." + count++ + "] Word : '" + m + "', starting at : " + m.Index);
            }

            return Regex.Replace(workStr, pattern2Replace, pattern2Replacement);
        }

        public string DoCommaSprinkler()
        {
            ModifiedStr = DoCommaSprinkler(OriginalStr);

            Log.Add("Original Text : " + OriginalStr);
            Log.Add("Modify Text   : " + ModifiedStr);

            if (ExpectedStr != null)
            {
                Log.Add("Expected Text : " + ExpectedStr);
                Log.Add("Result: Success = " + (ExpectedStr.Equals(ModifiedStr)));
            }

            return ModifiedStr;
        }

        private string DoCommaSprinkler(string workStr)
        {
            Log.Add("Iteration " + ++step + " Initiated. ######################################");
            string srcStr = workStr;

            int i = 0;
            Log.Add("[" + step + "] Looking For Comma After...");
            foreach (string s in FindWordsWithComma(workStr, PATTERN_COMMA_AFTER))
            {
                //"(" + s + ")" + (\\s | ^(\\.|\\,)) = "$1, "
                workStr = ModifyStr(workStr, "(" + s + ")" + PATTERN_MODIFY_COMMA_AFTER, PATTERN_REPLACEMENT_COMMA_AFTER);
                Log.Add("[" + step + "." + (i++) + "] Modified Str: " + workStr);
            }

            Log.Add("[" + step + "] Looking For Comma Before...");
            foreach (string s in FindWordsWithComma(workStr, PATTERN_COMMA_BEFORE))
            {
                // "(\\w)(\\s)" + "(" + s + ")" = "$1, $3"
                workStr = ModifyStr(workStr, PATTERN_MODIFY_COMMA_BEFORE + "(" + s + ")", PATTERN_REPLACEMENT_COMMA_BEFORE);
                Log.Add("[" + step + "." + (i++) + "] Modified Str: " + workStr);
            }

            Log.Add("Iteration " + step + " Finalized. ######################################");

            if (!srcStr.Equals(workStr))
            {
                workStr = DoCommaSprinkler(workStr);
            }

            return workStr;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyChange([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
