﻿using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace CommaSprinkler
{
    /// <summary>
    /// Group 5.
    /// </summary>
    public partial class MainWindow : Window
    {
        CommaSprinklerVM CsVM = new CommaSprinklerVM();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = CsVM;
        }

        private void Button_Do_Sprinkler(object sender, RoutedEventArgs e)
        {
            string tag = ((Button)sender).Tag.ToString();

            if (tag == "0")
            {
                string src = "please sit spot. sit spot, sit. spot here now here.";
                string strExpected = "please, sit spot. sit spot, sit. spot, here now, here.";
                CsVM = new CommaSprinklerVM(src, strExpected);
            }
            else if (tag == "1")
            {
                string src = "one, two. one tree. four tree. four four. five four. six five.";
                string strExpected = "one, two. one, tree. four, tree. four, four. five, four. six five.";
                CsVM = new CommaSprinklerVM(src, strExpected);
            }
            else if (tag == "2")
            {
                CsVM = new CommaSprinklerVM(CustomText.Text);
            }
            else if (tag == "3")
            {
                CustomText.Text = File.ReadAllText("input.txt");
                CsVM = new CommaSprinklerVM(CustomText.Text);
            }

            CsVM.DoCommaSprinkler();
            DataContext = CsVM;
        }
    }
}
